//
//  NCGRChatApp.swift
//  NCGRChat
//
//  Created by toc on 01/11/2022.
//

import SwiftUI

@main
struct NCGRChatApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
